## Run the project locally

Install cordova

    sudo npm install -g cordova

Checkout the project, change into the project directory and add a cordova platform (e.g. browser) and run the cordova app for that platform.

    cd talk/
    cordova platform add browser
    cordova run browser

For platform '''browser''' the app opens in default browser at '''http://localhost:8000/index.html'''.
