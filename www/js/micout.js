/*
    micout.js, v0.1
    by 
    [help by]
    https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia

    [description]
    [usage]

    [outlook]
*/
var MIC = (function(navigator) {
    console.log("called");

    console.log("Web Audio");
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    var context = new AudioContext();
    var source = null;

    navigator.getUserMedia = (navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia);

    navigator.mediaDevices.getUserMedia(
    {
        audio: true, 
        video: false
    }
    ).then(function(stream) {
        /* use the stream */
        source = context.createMediaStreamSource(stream);
        // is it required to connect a gainnode between source and destination???
        source.connect(context.destination);
    }).catch(function(err) {
        /* handle the error */
        console.log(err.name + ": " + err.message);
    });

    var mic = {};
    
    mic.connect = function(){
        source.connect(context.destination);
    }

    mic.disconnect = function(){
        source.disconnect();
    }

    return mic;

})(navigator);
